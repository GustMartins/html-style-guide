# HTML Style Guide

> “Toda linha de código deve parecer ter sido escrita por uma única pessoa, não importa o número de colaboradores.” —@mdo

Este documento define regras de formatação e estilo para HTML. Seu objetivo é melhorar a colaboração, a qualidade de código e habilitar um melhor suporte a uma infraestrutura aos projetos. Ele se aplica a todos os arquivos e trechos de código HTML, incluindo os em arquivo PHP.

## Índice

1. [Princípios gerais](#1-principios-gerais)
1.1. [Protocolo](#11-protocolo)
1.2. [Identação](#12-identacao)
1.3. [Capitalização](#13-capitalizacao)
1.4. [Espaços em branco](#14-espacos-em-branco)
1.5. [Codificação](#15-codificacao)
1.6. [Comentários](#16-comentarios)

2. [Regras de estilo](#2-regras-de-estilo)
2.1. [Tipo do documento](#21-tipo-do-documento)
2.2. [HTML válido](#22-html-valido)
2.3. [Semântica](#23-semantica)
2.4. [Multimídia](#24-multimidia)
2.5. [Separação de preocupações](#25-separacao-de-preocupacoes)
2.6. [Referências de entidade](#26-referencias-de-entidade)
2.7. [Tags opcionais](#27-tags-opcionais)
2.8. [Atributo `type`](#28-atributo-type)

3. [Regras de formatação](#3-regras-de-formatacao)
3.1. [Formatação geral](#31-formatacao-geral)
3.2. [Aspas em HTML](#32-aspas-em-html)
3.3. [Ordem dos atributos](#33-ordem-dos-atributos)
3.4. [Atributos booleanos](#34-atributos-booleanos)

4. [Considerações finais](#4-consideracoes-finais)


## 1. Princípios gerais

> "Parte de ser um bom gestor de um projeto bem sucedido é perceber que escrever código para si mesmo é uma má ideia. Se milhares de pessoas estão usando o seu código, então escreva-o com máxima clareza, não sob a sua preferência pessoal de como ser esperto com a especificação." - Idan Gazit

* Todo código em qualquer aplicação deve parecer como se tivesse sido escrito por uma única pessoa, independentemente de quantas pessoas tenham contribuído.
* Faça cumprir rigorosamente o estilo acordado.
* Em caso de dúvida, utilizar padrões existentes e comuns.

### 1.1. Protocolo

- Use o protocolo HTTPS para recursos externos quando possível.

> Sempre use o protocolo HTTPS (https:) para imagens e outros arquivos de mídia, arquivos de estilo, e _scripts_, a menos que o respectivo arquivo não esteja disponível no HTTPS.

```html
<!-- Não recomendado: omite o protocolo -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- Não recomendado: usa o protocolo HTTP -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<!-- Recomendado -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
```

### 1.2. Identação

- Use soft-tabs com dois espaços.

>  Não use tabs ou espaços com tabs para identação.

```html
<ul>
  <li>Fantástico</li>
  <li>Ótimo</li>
</ul>
```

### 1.3. Capitalização

- Use apenas texto em letra minúscula.

> Todo o código deve ser em letra minúscula. Isso se aplica a nomes de elementos HTML, atributos e valores de atrubutos.

```html
<!-- Não recomendado -->
<A HREF="/">Home</A>

<!-- Recomendado -->
<img src="google.png" alt="Google">
```

### 1.4. Espaços em branco

- Remova os espaços em branco.

> Espaços em branco são desnecessários e podem complicar diffs.

```html
<!-- Não recomendado -->
<p>What? </p>

<!-- Recomendado -->
<p>Yes please.</p>
```

### 1.5. Codificação

- Use UTF-8!
- Tenha certeza que seu editor usa UTF-8 como codificação para caracteres.

> Especifique a codificação nos arquivos e templates HTML usando `<meta charset="utf-8">`.

### 1.6. Comentários

Explique o código o quanto for necessário, quando possível.

Use os comentários para explicar no código:

- O que ele cobre
- A que propósito ele serve
- Porque a solução apresentada foi usada ou preferida

> (Este é um item opcional por que não é razoável concluir que todo o código será ou precisará ser comentado. Isso irá depender bastante da complexidade do projeto.)

## 2. Regras de estilo

### 2.1 Tipo do documento

- Use HTML5!

> HTML5 (HTML syntax) é o mais recomendado para todos os documentos HTML: `<!DOCTYPE html>`.
  É recomendado usar HTML, como `text/html`. Não use XHTML.

- Embora seja suportado no HTML, nunca inclua uma barra invertida nos elementos viúvos, i.e. escreva `<br>`, não `<br/>`.

### 2.2 HTML Válido

- Use HTML válido onde possível.

> Use ferramentas como [W3C HTML Validator](https://validator.w3.org/nu/) para testar o código.

> Usar HTML válido é um atributo de qualidade de linha de base mensurável que contribui para aprender sobre requisitos e restrições técnicas e que garante o uso correto de HTML.

```html
<!-- Não recomendado -->
<title>Teste</title>
<article>Isto é apenas um teste.

<!-- Recomendado -->
<!DOCTYPE html>
<meta charset="utf-8">
<title>Teste</title>
<article>Isto é apenas um teste.</article>
```

### 2.3 Semântica

- Use HTML de acordo com o seu propósito.

> Use elementos (algumas vezes chamados incorretamente de 'tags') para o que eles foram criados. Por exemplo, use elementos de cabeçalho para cabeçalhos, elementos p para parágrafos, elementos a para âncoras, etc.
Usar o HTML de acordo com o seu propósito é importante por razões de acessibilidade, reuso e eficiência de código.

```html
<!-- Não recomendado -->
<div onclick="irParaRecomendacoes();">Todas as recomendações</div>

<!-- Recomendado -->
<a href="recomendacoes/">Todas as recomendações</a>
```

### 2.4 Multimídia

- Forneça conteúdos alternativos para multimídia.

> Para multimídia, como imagens, vídeos, objetos animados via canvas, tenha certeza de oferecer um acesso alternativo. Para imagens isso significa usar um texto alternativo significativo (alt).
Fornecer conteúdos alternativos é importante por razões de acessibilidade: um usuário cego tem poucas dicas para saber do uma imagem se trata sem o atributo 'alt'.

```html
<!-- Não recomendado -->
<img src="spreadsheet.png">

<!-- Recomendado -->
<img src="spreadsheet.png" alt="Spreadsheet screenshot.">
```

### 2.5 Separação de Preocupações

- Separar a estrutura da apresentação do comportamento.

> Mantenha estritamente a estrutura (marcação), a apresentação (estilo) e o comportamento (scripts), e tente manter a interação entre os três para um mínimo absoluto.
Ou seja, certifique-se de que documentos e modelos contenham apenas HTML e HTML que serve apenas para fins estruturais. Mova tudo de apresentação para folhas de estilo, e tudo comportamental em scripts.
Separar a estrutura da apresentação do comportamento é importante por razões de manutenção. É sempre mais caro alterar documentos e modelos HTML do que atualizar folhas de estilo e scripts.

- Mantenha a área de contato tão pequena quanto possível, ligando poucas folhas de estilo e scripts de documentos e modelos.

```html
<!-- Não recomendado -->
<!DOCTYPE html>
<title>HTML chato</title>
<link rel="stylesheet" href="base.css" media="screen">
<link rel="stylesheet" href="grid.css" media="screen">
<link rel="stylesheet" href="print.css" media="print">
<h1 style="font-size: 1em;">HTML chato</h1>
<p>I’ve read about this on a few sites but now I’m sure: <u>HTML is stupid!!1</u>
<center>I can’t believe there’s no way to control the styling of my website without doing everything all over again!</center>

<!-- Recomendado -->
<!DOCTYPE html>
<title>My first CSS-only redesign</title>
<link rel="stylesheet" href="default.css">
<h1>My first CSS-only redesign</h1>
<p>I’ve read about this on a few sites but today I’m actually doing it: separating concerns and avoiding anything in the HTML of my website that is presentational.
<p>It’s awesome!
```

### 2.6 Referências de Entidade

- Não use referências de entidade.

> Não há razão para se usar referências de entidade como `&mdash;`, `&rdquo;`, ou `&#x263a;` assumindo que a mesma codificação (UTF-8) é usada entre todos na equipe em todos os arquivos e editores.

- A única excessão se aplica a caracteres que possuem significado especial em HTML (como `<` e `&`).

```html
<!-- Não recomendado -->
O símbolo da moeda do Euro é &ldquo;&eur;&rdquo;.

<!-- Recomendado -->
O símbolo da moeda do Euro é “€”.
```

### 2.7 Tags Opcionais

- Omita as tags opcionais (opcional).

> Para fins de otimização e dimensionamento de tamanho de arquivo considere omitir as tags opcionais. As [especificações do HTML5](https://html.spec.whatwg.org/multipage/syntax.html#optional-tags) define quais tags podem ser omitidas.

> (Essa abordagem pode exigir que um período de carência seja estabelecido como uma orientação mais ampla, pois é significativamente diferente do que os desenvolvedores da web geralmente são ensinados. Por razões de consistência e simplicidade, é melhor atendê-lo, omitiendo todas as tags opcionais, e não apenas uma seleção.)

```html
<!-- Não recomendado -->
<!DOCTYPE html>
<html>
  <head>
    <title>Gastando dinheiro, gastando bytes</title>
  </head>
  <body>
    <p>Sic.</p>
  </body>
</html>

<!-- Recomendado -->
<!DOCTYPE html>
<title>Poupando dinheiro, poupando bytes</title>
<p>Qed.
```

### 2.8 Atributo `type`

- Omita o atributo `type` dos arquivos de estilo e scripts.

> Especificar o atributo `type` nesses contextos não é necessário porque HTML5 implica `text/css` e `text/javascript` como padrão. Isso pode ser feito com segurança mesmo para navegadores mais antigos.

```html
<!-- Não recomendado -->
<link rel="stylesheet" href="https://www.google.com/css/maia.css" type="text/css">

<!-- Não recomendado -->
<script src="https://www.google.com/js/gweb/analytics/autotrack.js" type="text/javascript"></script>

<!-- Recomendado -->
<link rel="stylesheet" href="https://www.google.com/css/maia.css">

<!-- Recomendado -->
<script src="https://www.google.com/js/gweb/analytics/autotrack.js"></script>
```

## 3. Regras de Formatação

### 3.1 Formatação Geral

- Use uma nova linha para cada bloco, lista, ou elemento table, e idente cada elemento filho.

> Independentemente do estilo aplicado ao elemento use essa regra.

```html
<blockquote>
  <p><em>Espaço</em>, a fronteira final.</p>
</blockquote>

<ul>
  <li>Moe
  <li>Larry
  <li>Curly
</ul>

<table>
  <thead>
    <tr>
      <th scope="col">Fatura
      <th scope="col">Taxas
  <tbody>
    <tr>
      <td>$ 5.00
      <td>$ 4.50
</table>
```

### 3.2 Aspas em HTML

- Use aspas duplas ("") ao redor dos valores de atributos ao inves de aspas simples ('').

```html
<!-- Não recomendado -->
<a class='maia-button maia-button-secondary'>Cadastrar</a>

<!-- Recomendado -->
<a class="maia-button maia-button-secondary">Cadastrar</a>
```

### 3.3 Ordem dos atributos

- Atributos HTML devem usar esta ordem em particular para facilitar a leitura do código:

- `class`
- `id`
- `data-*`
- `for`, `type` ou `href`
- `src`, `for`, `type` ou `href`
- `title` ou `alt`
- `aria-*` ou `role`

### 3.4 Atributos booleanos

- Não declare valor a um atributo booleano.

> A presença de um atributo booleano em um elemento representa seu valor como verdadeiro, e a ausencia do atributo representa o valor falso.

```html
<!-- Não recomendado -->
<input type="text" disabled="disabled">

<!-- Não recomendado -->
<input type="checkbox" value="1" checked="checked">

<!-- Não recomendado -->
<select>
  <option value="1" selected="selected">1</option>
</select>

<!-- Recomendado -->
<input type="text" disabled>

<!-- Recomendado -->
<input type="checkbox" value="1" checked>

<!-- Recomendado -->
<select>
  <option value="1" selected>1</option>
</select>
```

## 4. Considerações Finais

__*Seja consistente*__!

Se você está editando um código, tire um tempinho para olhar o código ao redor e determinar seu estilo.

O ponto em ter um Guia de Estilo é possuir um vocabulário comum de código para que os membros da equipe se concentrem no que você está dizendo ao inves de em como está dizendo. Apresentamos aqui um *Guia de Estilo HTML Global* para que os membros da equipe saibam o vocabulário, mas melhorias são bem-vindas!
